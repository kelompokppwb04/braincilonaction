from django.db import models
from django.dispatch import receiver
from django.contrib.auth.models import User
from PIL import Image
# Create your models here.

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, default=0)
    image = models.ImageField(default="prof_pic/default.jpg", upload_to='prof_pic')
    title = models.CharField(max_length=50,default=0)
    jobs = models.CharField(max_length=50,default=0)
    contact = models.CharField(max_length=50,default=0)

    def save(self):
        super().save()

    def __str__(self):
        return self.user.username