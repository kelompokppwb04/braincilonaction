from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from django.http import HttpRequest
from .views import register, profilesetting
from .forms import MyUserCreationForm, ProfileSettingForm, ProfileSettingForm2, ProfilePictForm
from selenium import webdriver
from django.contrib.auth.models import User
from .models import Profile
# Create your tests here.

@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()
        
class RegisterUnitTestURL(TestCase):

    # Cek URL Register
    def test_apakah_ada_url_register(self):
        response = Client().get('/account/register/')
        self.assertEqual(response.status_code, 200)

    def test_apakah_view_dan_template_dari_path_kegiatan(self):
        response = self.client.get('/account/register/')
        found = resolve('/account/register/')
        self.assertEqual(found.view_name, "account:register")
        self.assertTemplateUsed(response, 'account/register.html')
        
class RegisterUnitTestHTML(TestCase):
    #Cek Isi HTML Kegiatan yang ada di views kegiatan + apakah method get pada views kegiatan menampilkan form kosong
    def test_isi_html_account_register(self):
        request = HttpRequest()
        response = register(request)
        html_kegiatan = response.content.decode('utf 8')
        self.assertIn("New to Braincil?", html_kegiatan)
        self.assertIn("Join Today!", html_kegiatan)
        self.assertIn("Username", html_kegiatan)
        self.assertIn("Email", html_kegiatan)
        self.assertIn("First Name", html_kegiatan)
        self.assertIn("Last Name", html_kegiatan)
        self.assertIn("Password", html_kegiatan)
        self.assertIn("Confirm Password", html_kegiatan)
        self.assertIn("Sign Up", html_kegiatan)

class RegisterUnitTestModel(TestCase):
    def setUp(self):
        User.objects.create(
            username="icil",
            email="kelompokppwb04@gmail.com",
            first_name="Kelompok PPW",
            last_name="B04",
            password="123456789PPW",
        )
    #Cek Apakah Model Kegiatan Sudah Terbuat
    def test_apakah_model_user_sudah_terbuat(self):
        jumlah_objek = User.objects.all().count()
        self.assertEquals(jumlah_objek, 1)

    # Cek field dari models
    def test_apakah_saat_membuat_objek_tersimpan_dengan_benar(self):
        objek_yang_dibuat = User.objects.get(username="icil")
        self.assertEquals(objek_yang_dibuat.username, "icil")
        self.assertEquals(objek_yang_dibuat.email, "kelompokppwb04@gmail.com")
        self.assertEquals(objek_yang_dibuat.first_name, "Kelompok PPW")
        self.assertEquals(objek_yang_dibuat.last_name, "B04")
        self.assertEquals(objek_yang_dibuat.password, "123456789PPW")

class RegisterUnitTestForm(TestCase):
    # Cek apakah form notpost valid
    def test_apakah_notpost_form_valid(self):
        form = MyUserCreationForm()
        self.assertTrue(form.is_valid)
    
    # Cek apakah form post valid
    def test_apakah_post_form_valid(self):
        form = MyUserCreationForm(data={
            'username':"icil",
            'email' : "kelompokppwb04@gmail.com",
            'first_name' : "Kelompok PPW",
            'last_name' : "B04",
            'password1' : "123456789PPW",
            'password2' : "123456789PPW",
        })
        self.assertTrue(form.is_valid)
    
    # Cek apakah objek terbuat saat ada isian form
    def test_apakah_objek_dapat_dibuat_dari_form(self):
        form = MyUserCreationForm(data={
            'username':"icil",
            'email' : "kelompokppwb04@gmail.com",
            'first_name' : "Kelompok PPW",
            'last_name' : "B04",
            'password1' : "123456789PPW",
            'password2' : "123456789PPW",
        })
        form.save()
        jumlah_objek_dari_form = User.objects.all().count()
        self.assertEqual(jumlah_objek_dari_form, 1)

class RegisterUnitTestView(TestCase):
        # Cek apakah post form dapat dilakukan
    def test_apakah_post_form_dapat_dilakukan(self):
        data = {
            'username':"icil",
            'email' : "kelompokppwb04@gmail.com",
            'first_name' : "Kelompok PPW",
            'last_name' : "B04",
            'password1' : "123456789PPW",
            'password2' : "123456789PPW",
        }
        response = Client().post('/account/register/', data)
        html_kegiatan = response.content.decode('utf 8')
        self.assertEqual(response.status_code, 302)

class ProfileUnitTestURL(TestCase):

    # Cek URL Register
    def test_apakah_ada_url_profile(self):
        response = Client().get('/account/profilesetting/')
        self.assertEqual(response.status_code, 302)

class ProfileUnitTestForm(TestCase):
    # Cek apakah form notpost valid
    def test_apakah_notpost_form_profile_valid(self):
        form1 = ProfileSettingForm()
        form2 = ProfileSettingForm2()
        form3 = ProfilePictForm()
        self.assertTrue(form1.is_valid)
        self.assertTrue(form2.is_valid)
        self.assertTrue(form3.is_valid)
    
    # Cek apakah form post valid
    def test_apakah_post_form_profile_valid(self):
        form1 = ProfileSettingForm(data={
            'username':"icil",
            'email' : "kelompokppwb04@gmail.com",
            'first_name' : "Kelompok PPW",
            'last_name' : "B04",
            'password1' : "123456789PPW",
            'password2' : "123456789PPW",
        })
        form2 = ProfileSettingForm2(data={
            'title' : 'S.Kom.',
            'jobs' : 'mahasiswa',
            'contact' : '087871833168',
        })
        self.assertTrue(form1.is_valid)
        self.assertTrue(form2.is_valid)
    
    # Cek apakah objek terbuat saat ada isian form
    def test_apakah_change_profile_dapat_dibuat_dari_form(self):
        form = ProfileSettingForm(data={
            'username':"icil",
            'email' : "kelompokppwb04@gmail.com",
            'first_name' : "Kelompok PPW",
            'last_name' : "B04",
            'password1' : "123456789PPW",
            'password2' : "123456789PPW",
        })
        form.save()
        jumlah_objek_dari_form = User.objects.all().count()
        self.assertEqual(jumlah_objek_dari_form, 1)
        
class ProfileUnitTestView(TestCase):
        # Cek apakah post form dapat dilakukan
    def test_apakah_post_form_profile_dapat_dilakukan(self):
        data = {
            'title' : 'S.Kom.',
            'jobs' : 'mahasiswa',
            'contact' : '087871833168',
        }
        response = Client().post('/account/profilesetting/', data)
        html_kegiatan = response.content.decode('utf 8')
        self.assertEqual(response.status_code, 302)
    
    def test_apakah_post_form_profilepict_dapat_dilakukan(self):
        data = {
            'image' : '/media/prof_pic/default.jpg'
        }
        response = Client().post('/account/profilesetting/', data)
        html_kegiatan = response.content.decode('utf 8')
        self.assertEqual(response.status_code, 302)