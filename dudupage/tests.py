from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from django.http import HttpRequest
from .views import index
from .models import duduModel
from .forms import duduForm
from selenium import webdriver
# Create your tests here.

@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()
        

class duduUnitTestURL(TestCase):

    # Cek URL LOGIN dan LOGOUT
    def test_apakah_ada_url_dudu(self):
        response = Client().get('/dudu/')
        self.assertEqual(response.status_code, 200)

    def test_nama_view_dan_template_dari_login(self):
        response = self.client.get('/dudu/')
        found = resolve('/dudu/')
        self.assertEqual(found.view_name, "dudupage:index")
        self.assertTemplateUsed(response, 'dudupage/dudupage.html')

class duduUnitTestHTML(TestCase):

    def test_isi_html_dudu(self):
        request = HttpRequest()
        response = index(request)
        dudupage = response.content.decode('utf 8')
        self.assertIn("apakah kamu punya pesan untuk seseorang ?", dudupage)
        self.assertIn("buat dudu kamu disini !", dudupage)
        self.assertIn("Dari :", dudupage)
        self.assertIn("Untuk:", dudupage)
        self.assertIn("Pesan :", dudupage)


class duduUnitTestModel(TestCase):

    def test_create_dudu(self):
        duduModel.objects.create(dari = 'aku')
        jumlah = duduModel.objects.all().count()
        self.assertGreaterEqual(jumlah, 1)


class duduUnitTestForm(TestCase):
    
    def test_form_valid(self):
        form = duduForm()
        self.assertTrue(form.is_valid)