from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

from .forms import duduForm
from .models import duduModel
# Create your views here.
@login_required
def index(request):
    
    postdudu = duduForm()

    if request.method == 'POST':
        duduModel.objects.create(
            dari 		= request.POST.get('dari'),
			untuk		= request.POST.get('untuk'),
			pesan	    = request.POST.get('pesan'),
        )

        return HttpResponseRedirect("/dudu/")
    

    getdudu = duduModel.objects.all()
    
    context = {
        'getdudu' : getdudu,
        'postdudu' : postdudu,
    }

    return render(request, 'dudupage/dudupage.html', context)
