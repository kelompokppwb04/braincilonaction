from django import forms

class LoginForm(forms.Form):

    error_messages = {
        'required' : 'Please Type',
        'unique': 'About user with this username already exists.',
        'password_mismatch' : "Password and confirm password don't match"
    }

    username_attrs = {
        'placeholder' : 'Username',
        'class' : 'form-control',
        'required' : True,
        'unique' : True
    }

    password1_attrs = {
        'placeholder' : 'Password',
        'class' : 'form-control',
        'required' : True,
    }

    username = forms.SlugField(widget=forms.TextInput(attrs=username_attrs))
    password1 = forms.CharField(widget=forms.PasswordInput(attrs=password1_attrs))