from django.urls import path

from . import views

app_name = 'loginpage'

urlpatterns = [
    path('login/', views.loginView, name='loginView'),
    path('logout/', views.logout_user, name='logout_user'),
]


