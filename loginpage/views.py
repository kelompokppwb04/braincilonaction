from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import LoginForm
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login,authenticate,logout

# Create your views here.
response={}
def loginView(request):
    form = LoginForm(request.POST)
    if request.method=='POST':
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
        
            if username=="" or password=="":
                messages.warning(request, form.errors)
            else:
                user = authenticate(username=username, password=password)
                print (user)
                if user : 
                    if user.is_active:
                        login(request, user)
                        return redirect('/')
                else:
                    messages.warning(request, "Invalid username or password")
                    return redirect ("/loginpage/login/")
    
    form = LoginForm()
    response['form'] = form
    return render(request, 'loginpage/login.html', response)

@login_required
def logout_user(request):
    logout(request)
    return redirect ('/')