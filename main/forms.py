from django import forms
from qna.models import Pertanyaan
from django.contrib.auth.models import User

class PertanyaanForm(forms.ModelForm):
    class Meta:
        model = Pertanyaan
        fields = ['matpel', 'pertanyaan']

    error_messages = {
        'required' : 'Please Type',
    }

    matpel_attrs = {
        'required' : True,
    }

    pertanyaan_attrs = {
        'placeholder' : 'Pertanyaan',
        'class' : 'form-control',
        'required' : True,
    }

    matpel = forms.ChoiceField(label="Mata Pelajaran", widget = forms.Select(),choices = ([
            ('Matematika', 'Matematika'),
            ('Fisika', 'Fisika'),
            ('Kimia', 'Kimia'),
            ('Biologi', 'Biologi'),
            ('IPS', 'IPS'),
            ('PKN', 'PKN'),
            ('Indonesia', 'Indonesia'), 
            ('Inggris', 'Inggris'),
            ('IT', 'IT'), ]))
    pertanyaan = forms.CharField(label="Pertanyaan", widget=forms.TextInput(attrs=pertanyaan_attrs))
    