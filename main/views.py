from django.shortcuts import render, redirect
from .forms import PertanyaanForm
from qna.models import Jawaban, Pertanyaan
from django.contrib.auth.decorators import login_required

response = {}
def home(request):
    form = PertanyaanForm(request.POST)
    if request.method=='POST' :
        if form.is_valid():
            pertanyaans = form.cleaned_data.get('pertanyaan')
            matpels = form.cleaned_data.get('matpel')
            Pertanyaan(usernanya=request.user, pertanyaan=pertanyaans, matpel=matpels).save()
            return redirect('/qna/question/')
    form = PertanyaanForm()
    response['form'] = form
    return render(request, 'main/home.html', response)

