from django import forms
from qna.models import Jawaban, Pertanyaan

class JawabanForm(forms.Form):
    error_messages = {
        'required' : 'Please Type',
    }

    jawaban_attrs = {
        'placeholder' : 'Mau jawab...',
        'class' : 'form-control',
        'required' : True,
    }

    jawaban = forms.CharField(label="Jawaban", widget=forms.TextInput(attrs=jawaban_attrs))
    