# Generated by Django 3.1.2 on 2020-11-21 09:40

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('qna', '0002_auto_20201121_1522'),
    ]

    operations = [
        migrations.AlterField(
            model_name='penjawab',
            name='pertanyaan',
            field=models.OneToOneField(default=0, on_delete=django.db.models.deletion.CASCADE, to='qna.penanya'),
        ),
    ]
