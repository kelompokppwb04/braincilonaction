from django.urls import path

from . import views

app_name = 'qna'

urlpatterns = [
    path('question/', views.question, name='question'),
    path('hasil/<int:id_det>/', views.hasil, name='hasil'),
]
