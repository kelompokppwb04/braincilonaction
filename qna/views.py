from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Pertanyaan, Jawaban
from .forms import JawabanForm
from django.contrib.auth.decorators import login_required

response={}
@login_required
def question(request):
    form = JawabanForm(request.POST)
    if request.method=='POST' :
        jawabans = request.POST.get("jawaban")
        pertanyaans = request.POST.get("p_id")
        question = Pertanyaan.objects.get(id=pertanyaans)
        Jawaban(userjawab=request.user, jawaban=jawabans, pertanyaan=question).save()
        return redirect('/qna/hasil/'+ str(pertanyaans))
    form = JawabanForm()
    response['form'] = form
    pertanyaan = Pertanyaan.objects.all()
    response['pertanyaan'] = pertanyaan
    return render (request, 'qna/question.html', response)

@login_required
def hasil(request, id_det):
    jawaban = Jawaban.objects.filter(pertanyaan__id=id_det)
    response['jawaban'] = jawaban
    return render (request, 'qna/hasil.html', response)


